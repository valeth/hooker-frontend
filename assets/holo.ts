interface Quote {
  author: String,
  text: String
}

class Quoter {
  static quotes: Quote[] = [
    {
      author: 'Coco Kiryu',
      text: 'If you wanna post Yagoo lewds... Go ahead and do it.'
    },
    {
      author: 'Aqua Minato',
      text: 'It\'s not a decoration!'
    },
    {
      author: 'Coco Kiryu',
      text: 'Ehem... Based. As. FUCK!'
    },
    {
      author: 'Mori Calliope',
      text: 'Hey! Fuck your friend! *random kazoo noises*'
    },
    {
      author: 'Pekora Usada',
      text: 'Time to simp!'
    },
    {
      author: 'Amelia Watson',
      text: 'We CAN ground-pound the spirit\'s mom!'
    },
    {
      author: 'Pekora Usada',
      text: 'Are you winning, son?'
    },
    {
      author: 'Amelia Watson',
      text: 'Shit on my face, brother...'
    },
    {
      author: 'Pekora Usada',
      text: 'SHADDAP, BITCH!'
    },
    {
      author: 'Moona Hoshinova',
      text: 'Can I have your sister instead? *police siren noises*'
    },
    {
      author: 'Calliope Mori',
      text: 'Fuck off, stupid bird, FUCK OFF!'
    },
    {
      author: 'Calliope Mori',
      text: '*seductively* You fucking piece of shit, you human garbage. I hope that your death is most painful.'
    },
    {
      author: 'Kiara Takanashi',
      text: 'Ame went to the YouTube office, and ground-pounded them real hard!'
    },
    {
      author: 'Amelia Watson',
      text: 'You watch your goddamn tone when you\'re talking to me.'
    },
    {
      author: 'Korone Inugami',
      text: 'Nevah gonna give you up. Nevah gonna let you down.'
    },
    {
      author: 'Gura Gawr',
      text: 'What\'s 10 + 10? 11.'
    },
    {
      author: 'Korone Inugami',
      text: 'I\'m Elvis Presley. I am justice. I am Volkswagen. I am Mercedes Benz. I am BMW. I am die, thank you foevah.'
    },
    {
      author: 'Coco Kiryu',
      text: 'Kill yourself, immediately!'
    },
    {
      author: 'Gura Gawr',
      text: 'Master-bait, here we go~'
    },
    {
      author: 'Various',
      text: 'I am horny!'
    },
    {
      author: 'Pekora Usada',
      text: 'Orrrrree!!!'
    },
    {
      author: 'Moona Hoshinova',
      text: 'nani mitendayo?!'
    }
  ]

  static pullRandom () {
    return Quoter.quotes[Math.floor(Math.random() * Quoter.quotes.length)]
  }
}

export default Quoter
