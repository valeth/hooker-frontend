export interface HookItemIFace {
  id?: String,
  description: String,
  gitlab_token: String,
  discord_url: String,
  created_at?: String
}

export class HookItem {
  raw: HookItemIFace

  id?: String
  description: String
  gitlab_token: String
  discord_url: String
  created_at?: String

  constructor (raw: HookItemIFace) {
    this.raw = raw
    this.id = this.raw.id
    this.description = this.raw.description
    this.gitlab_token = '*****'
    this.discord_url = this.getDiscordChannel()
    this.created_at = this.raw.created_at ? this.getDate() : undefined
  }

  getDate () {
    let date = new Date(this.raw.created_at as string)
    return date.toLocaleString()
  }

  getDiscordChannel () {
    const pieces = this.raw.discord_url.split('/')
    const idPiece = pieces[pieces.length - 2]
    return idPiece ? idPiece : '<Bad Discord URL>'
  }

  getHook () {
    return `${HookerAPI.hook}/${this.id}`
  }

  getToken () {
    return this.raw.gitlab_token
  }

  getDiscordUrl () {
    return this.raw.discord_url
  }
}

export class HookerAPI {
  static base = ""
  static api = `${HookerAPI.base}/api`
  static hook = `${HookerAPI.base}/hooks/gitlab`
  usr: String
  pwd: String
  
  constructor (usr: String, pwd: String) {
    this.usr = usr
    this.pwd = pwd
  }

  static saveCreds (usr: String, pwd: String) {
    let data = {'usr': usr, 'pwd': pwd}
    let jStr = JSON.stringify(data)
    let bsf = btoa(jStr)
    localStorage.setItem('credentials', bsf)
  }

  static loadCreds () {
    let data = {'usr': '', 'pwd': ''}
    let bsf = localStorage.getItem('credentials')
    if (bsf) {
      let jStr = atob(bsf)
      data = JSON.parse(jStr)
    }
    return data
  }

  authToken () {
    return `${this.usr}:${this.pwd}`
  }

  authHash () {
    return btoa(this.authToken())
  }

  authHeader () {
    return `Basic ${this.authHash()}`
  }

  headers () {
    let heads = new Headers()
    heads.append('Authorization', this.authHeader())
    heads.append('Content-Type', 'application/json')
    return heads
  }

  async get () {
    return await fetch(`${HookerAPI.api}/hooks`, {
      method: 'GET',
      headers: this.headers()
    })
  }

  async create (desc: String, token: String, hook: String) {
    return await fetch(`${HookerAPI.api}/hook`, {
      method: 'POST',
      headers: this.headers(),
      body: JSON.stringify({
        description: desc,
        gitlab_token: token,
        discord_url: hook
      })
    })
  }

  async delete (id: String) {
    await fetch(`${HookerAPI.api}/hook/${id}`, {
      method: 'DELETE',
      headers: this.headers()
    })
  }
}
